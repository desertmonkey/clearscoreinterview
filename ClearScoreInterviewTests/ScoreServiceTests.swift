//
//  ScoreServiceTests.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import XCTest

@testable import ClearScoreInterview

class ScoreServiceTests: XCTestCase, MockDataRequestable {
    
    func test_scoreFromJson() {
        
        let mockJSON = self.serializedMockJSON(fromFile: "MockResponse")!
        
        let sut = ScoreService()
        let result = sut.score(from: mockJSON)
        
        let expectedStatus = mockJSON["dashboardStatus"] as! String
        let scoreData = mockJSON["creditReportInfo"] as! [String: AnyObject]
        let expectedScore = scoreData["score"] as! Int
        let expectedMaxScore = scoreData["maxScoreValue"] as! Int
        
        XCTAssertEqual(result?.status, expectedStatus, "Score incorrectly parsed from JSON")
        XCTAssertEqual(result?.totalScore, expectedScore, "Score incorrectly parsed from JSON")
        XCTAssertEqual(result?.maxScore, expectedMaxScore, "Score incorrectly parsed from JSON")
    }
    
    func test_scoreViewModelFromJson() {
        
        let mockJSON = self.serializedMockJSON(fromFile: "MockResponse")!
        
        let sut = ScoreService()
        let result = sut.scoreViewModel(from: mockJSON)
        
        let expectedStatus = mockJSON["dashboardStatus"] as! String
        
        let scoreData = mockJSON["creditReportInfo"] as! [String: AnyObject]
        let expectedScore = String(scoreData["score"] as! Int)
        
        let maxScoreString = String(scoreData["maxScoreValue"] as! Int)
        let expectedMaxScore = "out of \(maxScoreString)"
        
        XCTAssertEqual(result?.status, expectedStatus, "Score View Model incorrectly parsed from JSON")
        XCTAssertEqual(result?.totalScore, expectedScore, "Score View Model incorrectly parsed from JSON")
        XCTAssertEqual(result?.maximumScore, expectedMaxScore, "Score View Model incorrectly parsed from JSON")
    }
    
}
