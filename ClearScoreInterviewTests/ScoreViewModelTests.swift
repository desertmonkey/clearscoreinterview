//
//  ScoreViewModelTests.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 18/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import XCTest

@testable import ClearScoreInterview

class ScoreViewModelTests: XCTestCase {
    
    func test_scorePercent_20Percent() {
        
        let totalScore = 20
        let maxScore = 100
        
        //expect 20%
        let expectation: Float = 0.2
        
        let score = Score(status: "Pass", totalScore: totalScore, maxScore: maxScore)
        let sut = ScoreViewModel(score: score)
        let result = sut.scorePercent
        
        XCTAssertEqual(result, expectation, "Incorrect score percent calculation")
    }
    
    func test_scorePercent_53Percent() {
        
        let totalScore = 546
        let maxScore = 1029
        
        let expectation: Float = 0.53
        
        let score = Score(status: "Pass", totalScore: totalScore, maxScore: maxScore)
        let sut = ScoreViewModel(score: score)
        let result = sut.scorePercent
        
        XCTAssertEqual(result, expectation, "Incorrect score percent calculation")
    }
    
    func test_maximumScore() {
        
        let maxScore = 45
        let expectation = "out of \(maxScore)"
        
        let score = Score(status: "Pass", totalScore: 20, maxScore: maxScore)
        let sut = ScoreViewModel(score: score)
        let result = sut.maximumScore
        
        
        XCTAssertEqual(result, expectation, "Incorrect max score label")
    }
}
