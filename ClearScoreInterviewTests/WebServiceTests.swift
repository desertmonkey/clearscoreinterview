//
//  WebServiceTests.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import XCTest

@testable import ClearScoreInterview

class WebServiceTests: XCTestCase {
    
    /**
     Test live API to see if non-nil data is returned...yes i am testing live data helps during dev!!!!
     */
    func test_requestForEndPointQueryItemsCompletion_sucess() {
        
        let expectation = self.expectation(description: "Webservice Request")
        let sut = WebService()
        
        sut.requestData(from: .score, queryItems: nil) { (result) in
            
            switch result {
            case .success(let json):
                XCTAssertNotNil(json, "API request results should not be nil!")
                break
            case .failure(_):
                XCTFail("API request results should not be nil!")
                break
            }
            
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5)
    }
    
    //MARK:- URL Creations
    
    //Following tests the url helper method to create url string from endpoint and query
    
    func test_urlWithEndPointQueryItems_1Query()
    {
        let sut = WebService()
        
        let queryItem = WebService.QueryItem("userID", "1")
        
        //create the expectation
        let endpoint = WebService.EndPoint.score
        let expectation = "\(sut.baseURLString)\(endpoint.rawValue)?\(queryItem.key)=\(queryItem.value)"
        
        let result = sut.url(with: endpoint, [queryItem])?.absoluteString
        XCTAssertEqual(result, expectation,"Incorrect url returned")
    }
    
    func test_urlWithEndPointQueryItems_noQuery()
    {
        let sut = WebService()
        
        //create the expectation
        let endpoint = WebService.EndPoint.score
        let expectation = "\(sut.baseURLString)\(endpoint.rawValue)"
        
        let result = sut.url(with: endpoint)?.absoluteString
        XCTAssertEqual(result, expectation,"Incorrect url returned")
    }
    
    //MARK:- Query Creations
    
    //Following test querys created correctly
    
    func test_queryFromQueryItems_1Query() {
        
        let sut = WebService()
        
        //create the expectation
        let queryItem = WebService.QueryItem("userID", "54")
        let expectation = [URLQueryItem(name: queryItem.key, value: queryItem.value)]
        
        let result = sut.query(from: [queryItem])
        XCTAssertEqual(result!, expectation,"Incorrect query returned")
        
    }
    
    func test_queryFromQueryItems_noQuerys() {
        
        let sut = WebService()
        let result = sut.query(from: [])
        XCTAssertNil(result, "Incorrect query returned")
    }
}
