//
//  DashboardViewController.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {
    
    @IBOutlet weak var scoreTitleLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var maximumScoreLabel: UILabel!
    @IBOutlet weak var graphView: ScoreGraphView!

    override func viewDidLoad() {
        super.viewDidLoad()
        applyLocalisation()
        updateScore()
    }
    
    /**
     Fetches the data from server and updates the score graph
     */
    func updateScore () {
        
        let service = ScoreService()
        service.fetchScore(){ score in
            
            //first ensure there is a scroe
            guard let score = score else { return }
            
            //next update the labels
            self.scoreTitleLabel.text = NSLocalizedString("Your credit score is", comment: "score title label")
            self.scoreLabel.text = score.totalScore
            self.maximumScoreLabel.text = score.maximumScore
            
            //and finally the graph view
            let percent = score.scorePercent
            self.graphView.showScore(with: percent)
        }
    }
    
    func applyLocalisation() {
        self.title = NSLocalizedString("Dashbaord", comment: "Dashboard Title")
        self.scoreTitleLabel.text = NSLocalizedString("Calculating score...", comment: "Score Loading text")
        
        //set labels to nil initially
        self.scoreLabel.text = nil
        self.maximumScoreLabel.text = nil
    }
}
