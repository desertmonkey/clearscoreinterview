//
//  ScoreViewModel.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import Foundation

struct ScoreViewModel {
    
    let score: Score
    
    var status: String? {
        return self.score.status
    }
    
    var totalScore: String? {
        return String(self.score.totalScore)
    }
    
    var maximumScore: String? {
        return "out of \(self.score.maxScore)"
    }
    
    var scorePercent: Float {
        
        let total = Float(self.score.totalScore)
        let max = Float(self.score.maxScore)
        let percent = total / max
        
        //Round to 2 decimal places
        let numberOfPlaces: Float = 2.0
        let multiplier: Float = pow(10.0, numberOfPlaces)
        let roundedPercent = round(percent * multiplier) / multiplier
        
        return roundedPercent
    }
}
