//
//  ScoreGraphView.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import UIKit

class ScoreGraphView: UIView {
    
    let loadingBubbleLayer = LoadingBubbleLayer()
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        //begin loading animation
        layer.addSublayer(loadingBubbleLayer)
        loadingAnimation()
    }
  
    func loadingAnimation() {
        loadingBubbleLayer.animate()
    }
    
    func showScore(with percent: Float) {
       loadingBubbleLayer.animateStroke(to: percent)
    }
}
