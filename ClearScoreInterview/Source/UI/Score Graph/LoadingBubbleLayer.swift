//
//  LoadingBubbleView.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import UIKit

class LoadingBubbleLayer: CAShapeLayer {
    
    let animationDuration = 0.5

    var center: CGPoint {
        guard let superlayer = superlayer else { return CGPoint(x: 0, y: 0) }
        let center = CGPoint(x: superlayer.bounds.midX, y: superlayer.bounds.midY)
        return center
    }
    
    var parentWidth: CGFloat {
        guard let superlayer = superlayer else { return 0.0 }
        return superlayer.frame.width
    }
    
    var parentHeight: CGFloat {
        guard let superlayer = superlayer else { return 0.0 }
        return superlayer.frame.height
    }
    
    override init() {
        
        super.init()
        
        //set inital bubble size
        path = smallBubblePath
        
        //set initial styling
        fillColor = UIColor.orange.cgColor
        lineWidth = 10.0
        strokeColor = UIColor.orange.cgColor
        strokeStart = 0.0
        strokeEnd = 0.0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK:- Animations

extension LoadingBubbleLayer {

    func animate() {
        
        //grow the bubble
        self.expand()
        //then begin the woble animation after it has grown
        Timer.scheduledTimer(timeInterval:animationDuration, target: self, selector: #selector(wobble), userInfo: nil, repeats: false)
    }
    
    /** 
     initial animation to 'grow' the bubble
     */
    func expand() {
        let expandAnimation: CABasicAnimation = CABasicAnimation(keyPath: "path")
        expandAnimation.fromValue = smallBubblePath
        expandAnimation.toValue = largeBubblePath
        expandAnimation.duration = animationDuration
        expandAnimation.fillMode = kCAFillModeForwards
        expandAnimation.isRemovedOnCompletion = false
        add(expandAnimation, forKey: nil)
    }
    
    /**
     Wobble animation which happens during the 'loading' phase
     */
    func wobble() {
        
        var animations = [CABasicAnimation]()
        var animationBeginTime = 0.0
        
        let wobbleAnimation = CABasicAnimation(keyPath: "path")
        wobbleAnimation.fromValue = largeBubblePath
        wobbleAnimation.toValue = bubbleSquishVertical(5)
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        //add as a copy so we can reuse
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        //update the animation begin time for the next animation
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        wobbleAnimation.fromValue = bubbleSquishVertical(5)
        wobbleAnimation.toValue = bubbleSquishHorizontal(10)
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        wobbleAnimation.fromValue = bubbleSquishHorizontal(10)
        wobbleAnimation.toValue = bubbleSquishVertical(-11)
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        wobbleAnimation.fromValue = bubbleSquishVertical(-11)
        wobbleAnimation.toValue = bubbleSquishHorizontal(-20)
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        wobbleAnimation.fromValue = bubbleSquishHorizontal(-20)
        wobbleAnimation.toValue = bubbleSquishVertical(25)
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        wobbleAnimation.fromValue = bubbleSquishVertical(25)
        wobbleAnimation.toValue = largeBubblePath
        wobbleAnimation.beginTime = animationBeginTime
        wobbleAnimation.duration = animationDuration
        animations.append(wobbleAnimation.copy() as! CABasicAnimation)
        
        animationBeginTime = wobbleAnimation.beginTime + wobbleAnimation.duration
        
        let wobbleAnimationGroup = CAAnimationGroup()
        wobbleAnimationGroup.animations = animations
        wobbleAnimationGroup.duration = animationBeginTime
        wobbleAnimationGroup.repeatCount = .infinity //will always repeat
        add(wobbleAnimationGroup, forKey: "wobble")
    }
    
    /**
     Called when the loading has finished and we want to animate the pogress bar
     */
    func animateStroke(to strokeEnd: Float) {
        
        //stop the wobble animation once we ready to show the progress
        removeAnimation(forKey: "wobble")
        
        //create a new circle, this will be the progress
        let progressLayer = CAShapeLayer()
        progressLayer.path = mediumBubblePath
        progressLayer.strokeStart = 0.0
        progressLayer.strokeEnd = 0.0
        //set the fill colour of the progress to match the parent, we will fade this to white
        progressLayer.fillColor = fillColor
        progressLayer.lineWidth = 8.0
        //progress color will be yellow
        progressLayer.strokeColor = UIColor.yellow.cgColor
        addSublayer(progressLayer)
        
        //create the color fade animation
        let colorAnimation: CABasicAnimation = CABasicAnimation(keyPath: "fillColor")
        colorAnimation.toValue = UIColor.white.cgColor
        colorAnimation.duration = 1.0
        colorAnimation.fillMode = kCAFillModeForwards
        colorAnimation.isRemovedOnCompletion = false
        
        //create the progress stoke animation, we will resue this for the parent stroke animation
        let stokeAnimation: CABasicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        stokeAnimation.fromValue = 0
        stokeAnimation.toValue = strokeEnd // set to the stoke percent argument (set from UI level)
        stokeAnimation.beginTime = colorAnimation.beginTime
        stokeAnimation.duration = 1.2 //this is slighly longer than the parent stroke animation
        stokeAnimation.fillMode = kCAFillModeForwards
        stokeAnimation.isRemovedOnCompletion = false
        
        let animationGroup = CAAnimationGroup()
        animationGroup.animations = [colorAnimation, stokeAnimation.copy() as! CAAnimation]
        animationGroup.duration = stokeAnimation.beginTime + stokeAnimation.duration
        animationGroup.repeatCount = 0
        animationGroup.fillMode = kCAFillModeForwards
        animationGroup.isRemovedOnCompletion = false
        //add the two animations to the porgress layer
        progressLayer.add(animationGroup, forKey: "showPercent")
        
        /*
         finally we adjust the stroke animation to go to 100%,
         this will be for the parent and not the progress
         */
        stokeAnimation.toValue = 1.0
        stokeAnimation.duration = 1.0
        add(stokeAnimation, forKey: "parentStroke")
    }
}

//MARK: - Convenience Methods for creating bubble paths

extension LoadingBubbleLayer {
    
    var smallBubblePath: CGPath {
        return UIBezierPath(ovalIn: CGRect(x: center.x, y: center.y, width: 0.0, height: 0.0)).cgPath
    }
    
    var mediumBubblePath: CGPath {
        return UIBezierPath(ovalIn: CGRect(x: center.x - (parentWidth/2), y: center.y - (parentHeight/2), width: parentWidth, height: parentHeight)).cgPath
    }
    
    var largeBubblePath: CGPath {
        return UIBezierPath(ovalIn: CGRect(x: center.x - (parentWidth/2), y: center.y - (parentHeight/2), width: parentWidth, height: parentHeight)).cgPath
    }
    
    func bubbleSquishVertical(_ squishAmount: CGFloat) -> CGPath {
        return UIBezierPath(ovalIn: CGRect(x: center.x - (parentWidth/2), y: center.y - (parentHeight/2) + squishAmount, width: parentWidth, height: parentHeight + squishAmount)).cgPath
    }
    
    func bubbleSquishHorizontal(_ squishAmount: CGFloat) -> CGPath {
        return UIBezierPath(ovalIn: CGRect(x: center.x - (parentWidth/2) + squishAmount, y: center.y - (parentHeight/2), width: parentWidth + squishAmount, height: parentHeight)).cgPath
    }
}
