//
//  WebService.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import Foundation

struct WebService {
    
    /**
     Query type to hold key/value of url query string args
     
     This is not a requirment of the test, however as it stated to be scaleable
     In all likeliness in production the request will take a query, such as
     a userID for the score we are retriveing
     
     the key could be refactored to be an enum once the types fo querys are defined
     
     */
    typealias QueryItem = (key: String, value: String)
    
    let baseURLString = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com"
    
    /**
     Makes a request for a given endpoint
     
     parameters:
     - endPoint: EndPoint enum
     - queryItems: Array of QueryItem objects, each with key/value of query
     - completion: callback to handle completion
     */
    func requestData(from endPoint: EndPoint, queryItems: [QueryItem]?, session: URLSession = URLSession.shared, completion: @escaping (RequestResult) -> Void) {
        
        //create the url
        guard let url = self.url(with: endPoint, queryItems) else {
            completion(.failure(.invalidURL))
            return
        }
        
        //kick off the request
        session.dataTask(with: url) { (data, response, error) in
            
            guard let data = data else {
                //return failure if data is nil
                completion(.failure(.nilData))
                return
            }
            
            do {
                //return the json result if success
                guard let json = try JSONSerialization.jsonObject(with: data) as? [String: AnyObject] else {
                    completion(.failure(.invalidJSON))
                    return
                }
                
                completion(.success(json))
                
            } catch {
                completion(.failure(.invalidJSON))
            }
            
            }.resume()
    }
}

//MARK:- Helper Functions

extension WebService {
    
    /**
     Constructs a url string from a given end point by
     appending the endpoint to the baseurl with any optional
     queries
     
     Parameters:
     - endPoint: endPoint enum
     - queryItems: Array of QueryItem objects, each with key/value of query
     
     returns: url: url string to be used in a request
     
     */
    func url(with endPoint: EndPoint, _ queryItems: [QueryItem]? = nil) -> URL? {
        
        //create the url with endPoint
        let urlString = "\(baseURLString)\(endPoint.rawValue)"
        var components = URLComponents(string:urlString)
        //create the query
        components?.queryItems = self.query(from: queryItems)
        
        guard let url = components?.url else { return nil }
        return url
    }
    
    /**
     Creates a URLQueryItem's array from an array of QueryItem's
     */
    func query(from queryItems: [QueryItem]?) -> [URLQueryItem]? {
        
        guard
            let queryItems = queryItems,
            queryItems.count > 0
            else { return nil }
        
        let urlQueryItems = queryItems.map{
            return URLQueryItem(name: $0.key, value: $0.value)
        }
        
        return urlQueryItems
    }
}


//MARK:- Request Enums

extension WebService {
    
    /**
     Result Types for request
     */
    enum RequestResult {
        
        case success([String: AnyObject])
        case failure(FailureReason)
    }
    
    /**
     Error Reasons for request
     */
    enum FailureReason: Error {
        
        case invalidURL
        case nilData
        case invalidJSON
    }
    
    /**
     End Points for rest API
     */
    enum EndPoint: String {
        case score = "/prod/mockcredit/values"
    }
}
