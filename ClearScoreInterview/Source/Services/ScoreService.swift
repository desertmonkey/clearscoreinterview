//
//  ScoreService.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import Foundation
struct ScoreService {
    
    /**
     Fetches all products for a given category
     */
    func fetchScore(service: WebService = WebService(), completion: @escaping (ScoreViewModel?) -> Void) {
        
        service.requestData(from: .score, queryItems: nil, session: URLSession.shared) { (result) in
            
            //will serve UI layer, so get main thread
            DispatchQueue.main.sync {
                switch (result) {
                    
                case .success (let json):
                    completion(self.scoreViewModel(from: json))
                    break
                    
                    /**
                     As test requried, can easily push error up to the UI layer from here.
                     Would require a slight refactor to check the types of failure based on 
                     score not being avialable due to errors such as incorrect user data etc
                     could possibly be better turning thing into a throws
                     */
                case .failure( _):
                    completion(nil)
                    break
                }
            }
        }
    }
    
    /**
     Parses and returns an array of Product Models in a completion block from a json response
     */
    func scoreViewModel(from json: [String: AnyObject]) -> ScoreViewModel? {
        
        guard let score = score(from: json) else { return nil }
        return ScoreViewModel(score: score)
    }
    
    /**
     Parses and returns an array of Product Models in a completion block from a json response
     */
    func score(from json: [String: AnyObject]) -> Score? {
        
        guard let scroeData = json["creditReportInfo"] as? [String: AnyObject] else { return  nil}
        
        guard
            let status = json["dashboardStatus"] as? String,
            let totalScore = scroeData["score"] as? Int,
            let maxScore = scroeData["maxScoreValue"] as? Int
            else { return nil }
        
        let score = Score(status: status, totalScore: totalScore, maxScore: maxScore)
        
        return score
    }
}
