//
//  Score.swift
//  ClearScoreInterview
//
//  Created by Shabeer Hussain on 14/12/2016.
//  Copyright © 2016 Desert Monkey. All rights reserved.
//

import Foundation

struct Score {
    
    let status: String
    let totalScore: Int
    let maxScore: Int
}
