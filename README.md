# Test Notes

### Dependencies

No third party libraries have been used.

In most cases CocoaPods would have been used to manage dependencies for the networking (most likley alamofire) and for the graph libraries.

Instead I have created everything from scratch to keep it simple. The networking layer is written around the URLSession class from Foundation. The graph is a custom implementation I wrote using Core Animation.

This will make it simpler to download and run on any machine.

### Testing

Added tests to cover all layers and business logic.

Didn't have time to fully test the integration between the Webservice and ScoreService layers (though have been tested individually).

The function signatures have been setup to use dependancy injection, however both will need to be moved to protocols to allow mocking of those objects (they are structs and can't use inheritance)

However easy enough to do so, and something I frequently do